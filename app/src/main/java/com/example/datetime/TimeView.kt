package com.example.datetime

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_time_view.*

class TimeView : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_time_view)

        val timer = Timer()
        lblTime.text = timer.time

        btnDateView.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}