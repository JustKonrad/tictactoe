package com.example.datetime;

import java.util.Random;

public class TicTacToe {

    private String[] _fields = {"", "", "", "", "", "", "", "", ""};
    private int _currentPlayer = 1;
    public String lastPlayer;
    private boolean isGameActive = true;

    public TicTacToe() {
        _setFirstPlayer();
    }

    public String getCurrentPlayer() {
        String current;
        if (_currentPlayer == 1) {
            current = "X";
        } else {
            current = "O";
        }

        return "Aktywnym graczem jest " + current;
    }

    private void _setFirstPlayer() {
        Random random = new Random();
        _currentPlayer = random.nextInt(3);
    }

    public void setFigureToField(int fieldNo) {
        fieldNo -= 1;
        if (isFieldEmpty(fieldNo) && isGameActive) {
            if (_currentPlayer == 1) {
                _fields[fieldNo] = "X";
                lastPlayer = "X";
            } else {
                _fields[fieldNo] = "O";
                lastPlayer = "O";
            }

            switchPlayer();
        }

    }

    private void switchPlayer() {
        if (_currentPlayer == 1) {
            _currentPlayer = 2;
        } else {
            _currentPlayer = 1;
        }
    }

    public boolean isFieldEmpty(int fieldNo) {

        return _fields[fieldNo] == "";
    }

    public String whoWin() {
        String message = "\nWygrał ";

        if (_fields[0] != "" && _fields[1] != "" && _fields[2] != "")
            if (_fields[0] == _fields[1] && _fields[1] == _fields[2]) {
                isGameActive = false;
                return message + _fields[0];
            }

        if (_fields[3] != "" && _fields[4] != "" && _fields[5] != "")
            if (_fields[3] == _fields[4] && _fields[4] == _fields[5]) {
                isGameActive = false;
                return message + _fields[3];
            }

        if (_fields[6] != "" && _fields[7] != "" && _fields[8] != "")
            if (_fields[6] == _fields[7] && _fields[7] == _fields[8]) {
                isGameActive = false;
                return message + _fields[0];
            }

        if (_fields[0] != "" && _fields[3] != "" && _fields[6] != "")
            if (_fields[0] == _fields[3] && _fields[3] == _fields[6]) {
                isGameActive = false;
                return message + _fields[0];
            }

        if (_fields[1] != "" && _fields[4] != "" && _fields[7] != "")
            if (_fields[1] == _fields[4] && _fields[4] == _fields[7]) {
                isGameActive = false;
                return message + _fields[1];
            }

        if (_fields[2] != "" && _fields[5] != "" && _fields[8] != "")
            if (_fields[2] == _fields[5] && _fields[5] == _fields[8]) {
                isGameActive = false;
                return message + _fields[2];
            }

        if (_fields[0] != "" && _fields[4] != "" && _fields[8] != "")
            if (_fields[0] == _fields[4] && _fields[4] == _fields[8]) {
                isGameActive = false;
                return message + _fields[0];
            }

        if (_fields[6] != "" && _fields[4] != "" && _fields[2] != "")
            if (_fields[6] == _fields[4] && _fields[4] == _fields[2]) {
                isGameActive = false;
                return message + _fields[0];
            }

        return "\nNarazie nikt nie wygrał";
    }

    public void clearBoard() {

        for (String figure: _fields) {
            figure = "";
        }
    }
}
