package com.example.datetime;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class Timer {

    private Calendar _cal;

    public Timer() {
        Date date = new Date();
        _cal = Calendar.getInstance(TimeZone.getDefault());
        _cal.setTime(date);
    }

    public String getTime() {
        int h = _cal.get(Calendar.HOUR_OF_DAY);
        int m = _cal.get(Calendar.MINUTE);

        String hour;
        String minutes;

        if(h < 10) {
            hour = "0" + String.valueOf(h);
        } else {
            hour = String.valueOf(h);
        }

        if (m < 10) {
            minutes = "0" + String.valueOf(m);
        } else {
            minutes = String.valueOf(m);
        }

        return hour + ":" + minutes;
    }

    public String getDate() {
        int d = _cal.get(Calendar.DAY_OF_MONTH);
        int m = _cal.get(Calendar.MONTH ) + 1;
        int y = _cal.get(Calendar.YEAR);

        String day;
        String month;
        String year;

        if (d < 10) {
             day = "0" + String.valueOf(d);
        } else {
            day = String.valueOf(d);
        }

        if (m < 10) {
            month = "0" + String.valueOf(m);
        } else {
            month = String.valueOf(m);
        }

        year = String.valueOf(y);

        return year + "-" + month + "-" + day;
    }
}
