package com.example.datetime

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.concurrent.timer

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val timer = Timer()
        lblDate.text = timer.date

        btnTimeView.setOnClickListener {
            startActivity(Intent(this, TimeView::class.java))
        }

        btnTicTacToe.setOnClickListener {
            startActivity(Intent(this, TicTacToeActivity::class.java))
        }
    }
}
