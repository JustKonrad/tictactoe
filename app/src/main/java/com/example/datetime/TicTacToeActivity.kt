package com.example.datetime

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_tic_tac_toe.*

class TicTacToeActivity : AppCompatActivity() {
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tic_tac_toe)

        val game = TicTacToe()
        game.clearBoard()
        btnBoard.text = game.currentPlayer

        btn1.setOnClickListener {
            game.setFigureToField(1)
            btn1.text = game.lastPlayer;
            btnBoard.text = game.currentPlayer + game.whoWin()
        }

        btn2.setOnClickListener {
            game.setFigureToField(2)
            btn2.text = game.lastPlayer
            btnBoard.text = game.currentPlayer + game.whoWin()
        }

        btn3.setOnClickListener {
            game.setFigureToField(3)
            btn3.text = game.lastPlayer
            btnBoard.text = game.currentPlayer + game.whoWin()
        }

        btn4.setOnClickListener {
            game.setFigureToField(4)
            btn4.text = game.lastPlayer
            btnBoard.text = game.currentPlayer + game.whoWin()
        }

        btn5.setOnClickListener {
            game.setFigureToField(5)
            btn5.text = game.lastPlayer
            btnBoard.text = game.currentPlayer + game.whoWin()
        }

        btn6.setOnClickListener {
            game.setFigureToField(6)
            btn6.text = game.lastPlayer
            btnBoard.text = game.currentPlayer + game.whoWin()
        }

        btn7.setOnClickListener {
            game.setFigureToField(7)
            btn7.text = game.lastPlayer
            btnBoard.text = game.currentPlayer + game.whoWin()
        }

        btn8.setOnClickListener {
            game.setFigureToField(8)
            btn8.text = game.lastPlayer
            btnBoard.text = game.currentPlayer + game.whoWin()
        }

        btn9.setOnClickListener {
            game.setFigureToField(9)
            btn9.text = game.lastPlayer
            btnBoard.text = game.currentPlayer + game.whoWin()
        }

        btnToStart.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}